extern printf
extern scanf

section .data

	out: db "Enter the number to print its multiplication table",10,0
	forstr: db "%d",0
	mult: db " %d * %d = %d",10,0
	inp: dd 0

section .text

	global main
	main:
	push ebp
	mov ebp,esp

	push out
	call printf
	push inp
	push forstr
	call scanf
	
	mov esi,1
	
	loop:
	mov ebx,esi
	imul ebx,[inp]
	
	push ebx
	push dword[inp]
	push esi
	push mult
	call printf

	inc esi
	
	cmp esi,13
	jne loop

	leave
	ret

