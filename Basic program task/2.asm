extern printf
extern scanf

section .data

	inp: dd 0
	out: db "Enter the number",10,0
	inp1: db "%d",0

section .text
	
	global main
	main:
	push ebp
	mov ebp,esp

	push out
	call printf

	push inp
	push inp1
	call scanf

	mov ebx,0
	mov eax,1
	
	add  [inp],eax
	
	l1:
	add ebx,eax
	inc eax
	cmp eax,[inp] 
	jne l1
	
	push ebx
	push inp1
	call printf

	leave 
	ret

