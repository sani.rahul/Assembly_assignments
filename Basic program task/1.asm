extern printf
extern scanf

section .data

        greet:db "Hello %s ,Welcome to the workspace ",10,0
        out:db "%s",0
        inp:db 0
        greet2:db "Enter your name",10,0
section .text

        global main
        main:

        push ebp
        mov ebp,esp

        push greet2
        call printf

        push inp
        push out
        call scanf
	
	push inp
	push greet
	call printf

	leave
	ret

