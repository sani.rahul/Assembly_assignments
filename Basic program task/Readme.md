

# Droid_vuln

### Description

Few years before a major vulnerability was found on all devices running on particular type of Samsung processors .This vulnerability was discovered by a XDA-Developers forum member Aleph Zain .The security flaw is actually in the kernel which makes the device Read/Write by all users, apps and gives access to full Physical Memory.

Can you guys find out the  processor model number that had this vulnerability.

>If there are multiple processors then enter the model numbers  in ascending order 
 >inctfj{<123_456>}

###### Difficulty level ⇒ 2

#### Flag : inctfj{4210_4412}

---------------------------------------------

How to solve this challenge 

First when we google “aleph zain Samsung vulnerability”,THe first result itself is “Samsung devices vulnerable to dangerous Android exploit ..” when the link is opened an article will be shown , if they read it they will get to know the model numbers





